#include "libcl.h"

int		main(void)
{
	char		hello[15];
	cl_int		buffer;
	cl_int		kernel;
	const char	*kernel_file[] = {"hello.cl"};

	cl_init();
	kernel = cl_create_kernel(kernel_file, 1, "kern");
	buffer = cl_create_empty_buffer(CL_MEM_WRITE_ONLY, sizeof(hello));
	cl_set_buffer_arg(kernel, 0, buffer);
	cl_exec_kernel(kernel, 1);
	cl_read_buffer(buffer, hello, 0, sizeof(hello));
	cl_finish_kernel_exec();
	printf("%s", hello);
	return (EXIT_SUCCESS);
}
