#include "test.h"

void	test_cl_set_value_arg()
{
	cl_int		kernel;
	int			value = 42;
	const char	*files[] =
	{
		"sources/kernel/test_1.cl"
	};

	init_test("cl_set_value_arg");
	cl_init();
	kernel = cl_create_kernel(files, 1, "test_3");
	test(cl_set_value_arg(128, -1, 0, NULL), -1);
	test(cl_set_value_arg(kernel, -128, 0, NULL), -1);
	test(cl_set_value_arg(kernel, 0, 0, &value), -1);
	test(cl_set_value_arg(kernel, 1, sizeof(int), &value), 0);
	test(cl_set_value_arg(kernel, 0, sizeof(int), &value), -1);
	test(cl_set_value_arg(1, 0, sizeof(int), &value), -1);
	test(cl_set_value_arg(kernel, 128, sizeof(int), &value), -1);
	test(cl_set_value_arg(kernel, 1, sizeof(int), &value), 0);
	cl_delete_kernel(kernel);
	cl_exit();
}
