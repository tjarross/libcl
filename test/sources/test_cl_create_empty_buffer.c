#include "test.h"

void	test_cl_create_empty_buffer()
{
	init_test("cl_create_empty_buffer");
	cl_init();
	test(cl_create_empty_buffer(CL_MEM_WRITE_ONLY, 0), -1);
	test(cl_create_empty_buffer(CL_MEM_WRITE_ONLY, 0), -1);
	test(cl_create_empty_buffer(CL_MEM_WRITE_ONLY, 42), 0);
	test(cl_create_empty_buffer(CL_MEM_READ_ONLY, 42), 1);
	test(cl_create_empty_buffer(CL_MEM_READ_WRITE, 42), 2);
	test(cl_create_empty_buffer(-1, 42), -1);
	cl_delete_buffer(1);
	test(cl_create_empty_buffer(CL_MEM_READ_WRITE, 42), 1);
	cl_delete_buffer(0);
	test(cl_create_empty_buffer(CL_MEM_READ_WRITE, 42), 0);
	cl_delete_buffer(1);
	cl_delete_buffer(0);
	cl_delete_buffer(2);
	cl_exit();
}
