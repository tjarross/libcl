#include "test.h"

void	test_cl_read_buffer()
{
	cl_int	buffer;
	char	*str = malloc(42);

	init_test("cl_read_buffer");
	cl_init();
	test(cl_read_buffer(128, NULL, 0, 0), -1);
	test(cl_read_buffer(-1, NULL, 0, 0), -1);
	buffer = cl_create_empty_buffer(CL_MEM_READ_ONLY, 42);
	test(cl_read_buffer(buffer, NULL, 0, 0), -1);
	test(cl_read_buffer(buffer, str, 0, 0), -1);
	test(cl_read_buffer(buffer, str, 0, 43), -1);
	test(cl_read_buffer(buffer, str, 0, 42), 0);
	test(cl_read_buffer(buffer, NULL, 0, 42), -1);
	test(cl_read_buffer(buffer, str, 42, 42), -1);
	test(cl_read_buffer(buffer, str, 41, 42), -1);
	test(cl_read_buffer(buffer, str, 43, 42), -1);
	test(cl_read_buffer(buffer, str, 43, 45), -1);
	cl_delete_buffer(buffer);
	cl_exit();
}
