#include "test.h"

void	test_cl_buffer_count()
{
	char	*str = malloc(42);

	init_test("cl_buffer_count");
	cl_init();
	cl_create_buffer(NULL, CL_MEM_WRITE_ONLY, 0);
	test(cl.ext_var.cl_buffer_count, 0);
	cl_create_buffer(str, CL_MEM_WRITE_ONLY, 0);
	test(cl.ext_var.cl_buffer_count, 0);
	cl_create_buffer(str, CL_MEM_WRITE_ONLY, 42);
	test(cl.ext_var.cl_buffer_count, 1);
	cl_create_buffer(NULL, CL_MEM_READ_ONLY, 42);
	test(cl.ext_var.cl_buffer_count, 1);
	cl_create_buffer(str, CL_MEM_READ_WRITE, 42);
	test(cl.ext_var.cl_buffer_count, 2);
	cl_create_buffer(str, -1, 42);
	test(cl.ext_var.cl_buffer_count, 2);
	cl_delete_buffer(1);
	test(cl.ext_var.cl_buffer_count, 1);
	cl_create_buffer(str, CL_MEM_READ_WRITE, 42);
	test(cl.ext_var.cl_buffer_count, 2);
	cl_delete_buffer(0);
	test(cl.ext_var.cl_buffer_count, 2);
	cl_create_buffer(str, CL_MEM_READ_WRITE, 42);
	test(cl.ext_var.cl_buffer_count, 2);
	cl_delete_buffer(1);
	test(cl.ext_var.cl_buffer_count, 1);
	cl_delete_buffer(0);
	test(cl.ext_var.cl_buffer_count, 0);

	cl_create_empty_buffer(CL_MEM_WRITE_ONLY, 0);
	test(cl.ext_var.cl_buffer_count, 0);
	cl_create_empty_buffer(CL_MEM_WRITE_ONLY, 0);
	test(cl.ext_var.cl_buffer_count, 0);
	cl_create_empty_buffer(CL_MEM_WRITE_ONLY, 42);
	test(cl.ext_var.cl_buffer_count, 1);
	cl_create_empty_buffer(CL_MEM_READ_ONLY, 42);
	test(cl.ext_var.cl_buffer_count, 2);
	cl_create_empty_buffer(CL_MEM_READ_WRITE, 42);
	test(cl.ext_var.cl_buffer_count, 3);
	cl_create_empty_buffer(-1, 42);
	test(cl.ext_var.cl_buffer_count, 3);
	cl_delete_buffer(1);
	test(cl.ext_var.cl_buffer_count, 3);
	cl_create_empty_buffer(CL_MEM_READ_WRITE, 42);
	test(cl.ext_var.cl_buffer_count, 3);
	cl_delete_buffer(0);
	test(cl.ext_var.cl_buffer_count, 3);
	cl_create_empty_buffer(CL_MEM_READ_WRITE, 42);
	test(cl.ext_var.cl_buffer_count, 3);
	cl_delete_buffer(1);
	test(cl.ext_var.cl_buffer_count, 3);
	cl_delete_buffer(0);
	test(cl.ext_var.cl_buffer_count, 3);
	cl_delete_buffer(2);
	test(cl.ext_var.cl_buffer_count, 0);
	free(str);
	cl_exit();
}
