#include "test.h"

void	summary(void)
{
	printf("\n\033[0;32m%d\033[0;29m successful tests and \033[0;31m%d\033[0;29m failed tests\n", t.successful_tests, t.failed_tests);
}

int		main(void)
{
	bzero(&t, sizeof(t_test));
	test_cl_init();
	test_cl_create_kernel();
	test_cl_kernel_count();
	test_cl_delete_kernel();
	test_cl_create_buffer();
	test_cl_buffer_count();
	test_cl_create_empty_buffer();
	test_cl_delete_buffer();
	test_cl_set_buffer_arg();
	test_cl_set_value_arg();
	test_cl_exec_kernel();
	test_cl_read_buffer();
	test_cl_finish_kernel_exec();
	test_cl_exit();

#ifdef LIBCL_ENABLE_INTEROP
	test_cl_interop_init();
	test_cl_create_gl_buffer();
	test_gl_buffer_count();
	test_cl_delete_gl_buffer();
	test_cl_set_gl_buffer_arg();
	test_cl_set_gl_buffer_arg();
	test_cl_exec_interop_kernel();
#endif /* LIBCL_ENABLE_INTEROP */
	finish_tests();
	summary();
}
