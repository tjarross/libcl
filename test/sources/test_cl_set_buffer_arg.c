#include "test.h"

void	test_cl_set_buffer_arg()
{
	cl_int		kernel;
	cl_int		buffer;
	const char	*files[] =
	{
		"sources/kernel/test_1.cl"
	};

	init_test("cl_set_buffer_arg");
	cl_init();
	kernel = cl_create_kernel(files, 1, "test_3");
	test(cl_set_buffer_arg(128, -1, 1), -1);
	test(cl_set_buffer_arg(kernel, -128, 1), -1);
	test(cl_set_buffer_arg(kernel, 0, 0), -1);
	buffer = cl_create_empty_buffer(CL_MEM_READ_ONLY, 42);
	test(cl_set_buffer_arg(kernel, 0, buffer), 0);
	test(cl_set_buffer_arg(1, 0, buffer), -1);
	test(cl_set_buffer_arg(kernel, 128, buffer), -1);
	test(cl_set_buffer_arg(kernel, 1, buffer), -1);
	cl_delete_buffer(buffer);
	cl_delete_kernel(kernel);
	cl_exit();
}
