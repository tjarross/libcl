#include "test.h"

FILE *fp = NULL;

void	init_test(const char *function)
{
	t.counter = 0;
	strncpy(t.f_name, function, MAX_F_NAME);
}

void	test(int f_ret, int expected_ret)
{
	++t.counter;
	if (f_ret != expected_ret)
	{
		if (!fp)
			fp = fopen("libcl_test.log", "w");
		printf("%d %s \033[0;31mFAIL\033[0;29m", t.counter, t.f_name);
		++t.failed_tests;
		fprintf(fp, "%d %s return : %d expected : %d\n", t.counter, t.f_name, f_ret, expected_ret);
	}
	else
	{
		printf("%d %s \033[0;32mOK\033[0;29m", t.counter, t.f_name);
		++t.successful_tests;
	}
	printf("\n");
}

void	finish_tests()
{
	if (fp)
		fclose(fp);
}
