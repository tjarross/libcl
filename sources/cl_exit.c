#include "libcl.h"

int		cl_exit(void)
{
	int		i = 0;

	if (cl.ext_var.exception_fp)
	{
		fclose(cl.ext_var.exception_fp);
		cl.ext_var.exception_fp = NULL;
	}
	clFinish(cl.command_queue);
	clFlush(cl.command_queue);
	while (i < cl.ext_var.kernel_count && cl.kernel)
		clReleaseKernel(cl.kernel[i++]);
	if (cl.kernel)
		free(cl.kernel);
	clReleaseProgram(cl.program);
	i = 0;
	while (i < cl.ext_var.cl_buffer_count && cl.cl_buffer)
		clReleaseMemObject(cl.cl_buffer[i++]);
	if (cl.cl_buffer)
		free(cl.cl_buffer);
#ifdef LIBCL_ENABLE_INTEROP
	i = 0;
	while (i < cl.ext_var.gl_buffer_count && cl.gl_buffer)
		clReleaseMemObject(cl.gl_buffer[i++]);
	if (cl.gl_buffer)
		free(cl.gl_buffer);
#endif /* LIBCL_ENABLE_INTEROP */
	i = 0;
	clReleaseCommandQueue(cl.command_queue);
	clReleaseContext(cl.context);
	return (0);
}
