#include "libcl.h"

#ifdef LIBCL_ENABLE_INTEROP

int		cl_delete_gl_buffer(cl_int buffer)
{
	if (!cl.ext_var.gl_buffer_count || buffer >= cl.ext_var.gl_buffer_count)
		return (-1);
	if ((cl.ret = clReleaseMemObject(cl.gl_buffer[buffer])) != CL_SUCCESS)
		return (cl_exception("clReleaseMemObj() Error: ", cl.ret));
	cl.gl_buffer[buffer] = NULL;
	while (cl.ext_var.gl_buffer_count > 0 && cl.gl_buffer[cl.ext_var.gl_buffer_count - 1] == NULL)
		--cl.ext_var.gl_buffer_count;
	if (!cl.ext_var.gl_buffer_count)
	{
		free(cl.gl_buffer);
		cl.gl_buffer = NULL;
	}
	return (0);
}

#endif /* LIBCL_ENABLE_INTEROP */

int		cl_delete_buffer(cl_int buffer)
{
	if (!cl.ext_var.cl_buffer_count || buffer >= cl.ext_var.cl_buffer_count)
		return (-1);
	if ((cl.ret = clReleaseMemObject(cl.cl_buffer[buffer])) != CL_SUCCESS)
		return (cl_exception("clReleaseMemObj() Error: ", cl.ret));
	cl.cl_buffer[buffer] = NULL;
	while (cl.ext_var.cl_buffer_count > 0 && cl.cl_buffer[cl.ext_var.cl_buffer_count - 1] == NULL)
		--cl.ext_var.cl_buffer_count;
	if (!cl.ext_var.cl_buffer_count)
	{
		free(cl.cl_buffer);
		cl.cl_buffer = NULL;
	}
	return (0);
}
