#include "libcl.h"

#ifdef LIBCL_ENABLE_INTEROP

int		cl_exec_interop_kernel(cl_int kernel, size_t cores)
{
	if (kernel < 0 || kernel >= cl.ext_var.kernel_count || !cores)
		return (-1);
	glFinish();
	if ((cl.ret = clEnqueueAcquireGLObjects(cl.command_queue, cl.ext_var.gl_buffer_count, cl.gl_buffer, 0, NULL, NULL)) != CL_SUCCESS)
		return (cl_exception("clEnqueueAcquireGLObjects() Error: ", cl.ret));
	if ((cl.ret = clEnqueueNDRangeKernel(cl.command_queue, cl.kernel[kernel], 1, NULL, &cores, NULL, 0, NULL, NULL)) != CL_SUCCESS)
		return (cl_exception("clEnqueueNDRangeKernel() Error: ", cl.ret));
	if ((cl.ret = clEnqueueReleaseGLObjects(cl.command_queue, cl.ext_var.gl_buffer_count, cl.gl_buffer, 0, NULL, NULL)) != CL_SUCCESS)
		return (cl_exception("clEnqueueReleaseGLObjects() Error: ", cl.ret));
	return (0);
}

#endif /* LIBCL_ENABLE_INTEROP */

int		cl_exec_kernel(cl_int kernel, size_t cores)
{
	if (kernel < 0 || kernel >= cl.ext_var.kernel_count || !cores)
		return (-1);
	if ((cl.ret = clEnqueueNDRangeKernel(cl.command_queue, cl.kernel[kernel], 1, NULL, &cores, NULL, 0, NULL, NULL)) != CL_SUCCESS)
		return (cl_exception("clEnqueueNDRangeKernel() Error: ", cl.ret));
	return (0);
}

int		cl_finish_kernel_exec()
{
	if ((cl.ret = clFinish(cl.command_queue)) != CL_SUCCESS)
		return (cl_exception("clFinish() Error: ", cl.ret));
	return (0);
}
