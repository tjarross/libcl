#include "libcl.h"

int		cl_delete_kernel(cl_int kernel)
{
	if (kernel < 0 || kernel >= cl.ext_var.kernel_count)
		return (-1);
	if ((cl.ret = clReleaseKernel(cl.kernel[kernel])) != CL_SUCCESS)
		return (cl_exception("clReleaseKernel() Error: ", -1));
	cl.kernel[kernel] = NULL;
	while (cl.ext_var.kernel_count > 0 && cl.kernel[cl.ext_var.kernel_count - 1] == NULL)
		--cl.ext_var.kernel_count;
	if (!cl.ext_var.kernel_count)
	{
		free(cl.kernel);
		cl.kernel = NULL;
	}
	return (0);
}
