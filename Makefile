OS = $(shell uname -s)
LIB =		libcl.a
SRC_NAME =	cl_init.c \
			cl_exit.c \
			cl_exception.c \
			cl_create_kernel.c \
			cl_delete_kernel.c \
			cl_create_buffer.c \
			cl_delete_buffer.c \
			cl_set_kernel_arg.c \
			cl_exec_kernel.c \
			cl_read_buffer.c
SRC =		$(addprefix sources/, $(SRC_NAME))
OBJ =		$(SRC:.c=.o)
INC =		-I ./includes \
			-I $(HOME)/.brew/Cellar/glfw/3.2.1/include \
			-I /usr/local/Cellar/glfw/3.2.1/include
CC =		gcc
FLAGS = 	-Ofast -Wall -Werror -Wextra $(INC)
ifeq ($(OS), Darwin)
EXT_LIB =	-framework OpenCL \
			-framework OpenGL \
			`pkg-config --static --libs glfw3`
else
EXT_LIB =	-lOpenCL \
			-lGL \
			`pkg-config --static --libs glfw3`
endif

all: $(LIB)

msg:
	@echo "\n\033[29m⌛  Making Libcl : \c"

$(LIB): msg $(OBJ)
	@ar rcs $(LIB) $(OBJ)
	@ranlib $(LIB)
	@echo "\n\033[34m✅  Libcl Created !\033[0;29m"

%.o: %.c
	@$(CC) $(FLAGS) -o $@ -c $<
	@echo "\033[32m.\033[0;29m\c"

clean:
	@echo "\033[31m🔥  Cleaning Libcl Objects..."
	@rm -f $(OBJ)

fclean: clean clean_test
	@echo "\033[31m🔥  Cleaning Libcl Library...\033[0;29m"
	@rm -f $(LIB)

re: fclean all

test: all
	@make -sC test

clean_test:
	@make fclean -sC test

.PHONY: all clean fclean re msg test clean_test
